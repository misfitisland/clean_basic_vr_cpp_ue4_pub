// Copyright Epic Games, Inc. All Rights Reserved.

#include "Ephemeral.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Ephemeral, "Ephemeral" );
