// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EphemeralGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EPHEMERAL_API AEphemeralGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
