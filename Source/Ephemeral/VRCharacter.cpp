// Fill out your copyright notice in the Description page of Project Settings.


#include "VRCharacter.h"

#include "HandController.h"

#include "DrawDebugHelpers.h"
#include "NavigationSystem.h"

#include "Camera/CameraComponent.h"
#include "Components/InstancedStaticMeshComponent.h" 
#include "Components/PostProcessComponent.h" 
#include "Components/SplineComponent.h" 
#include "Components/SplineMeshComponent.h" 
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h" 
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h" 
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"

#include <sstream>

// Sets default values
AVRCharacter::AVRCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// We're the only player so, bam
	_playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	_vrRoot = CreateDefaultSubobject<USceneComponent>(L"VRRoot");
	_vrRoot->SetupAttachment(GetRootComponent());

	_camera = CreateDefaultSubobject<UCameraComponent>(L"Camera");
	_camera->SetupAttachment(_vrRoot);

	_destinationMarker = CreateDefaultSubobject<UStaticMeshComponent>(L"DestinationMarker");
	_destinationMarker->SetupAttachment(GetRootComponent());

	_postProcessComponent = CreateDefaultSubobject<UPostProcessComponent>(L"PostProcessComponent");
	_postProcessComponent->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void AVRCharacter::BeginPlay()
{
	Super::BeginPlay();

	_destinationMarker->SetHiddenInGame(true, true);

	_leftController = GetWorld()->SpawnActor<AHandController>(_leftHandControllerBP);
	if(_leftController)
	{
		_leftController->AttachToComponent(_vrRoot, FAttachmentTransformRules::KeepRelativeTransform);
		_leftController->SetOwner(this);
		_leftController->SetupControllerBindings(_playerInputComponent);
		_leftController->OnGripStart.BindUFunction(this, L"GripStart");
		_leftController->OnGripEnd.BindUFunction(this, L"GripEnd");

		_teleportPath = NewObject<USplineComponent>(this, L"TeleportPath");
		_teleportPath->AttachToComponent(_leftController->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
		_teleportPath->RegisterComponent();
	}

	_rightController = GetWorld()->SpawnActor<AHandController>(_rightHandControllerBP);
	if(_rightController)
	{
		_rightController->AttachToComponent(_vrRoot, FAttachmentTransformRules::KeepRelativeTransform);
		_rightController->SetOwner(this);
		_rightController->SetupControllerBindings(_playerInputComponent);
		_rightController->OnGripStart.BindUFunction(this, L"GripStart");
		_rightController->OnGripEnd.BindUFunction(this, L"GripEnd");
	}

	if(_blinkerMaterialBase)
	{
		_blinkerInstance = UMaterialInstanceDynamic::Create(_blinkerMaterialBase, this);
		_blinkerInstance->SetScalarParameterValue(L"Radius", 0.0f);

		_postProcessComponent->AddOrUpdateBlendable(_blinkerInstance);
		_postProcessComponent->bUnbound = true;
	}

	// Save actor height for useful later
	FVector bounds;
	FVector orig;
	GetActorBounds(true, orig, bounds);
	_actorHalfHeight = bounds.Z;
}

// Called every frame
void AVRCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateVRRoot();
	
	if(_isCastingTeleport)
		CastTeleportDestinationMarker();

	if(!_isClimbing)
		UpdateBlinkers();
	else
	{
		FVector moveDelta {0,0,0};
		if(_leftController->IsGripping() && _rightController->IsGripping())
			moveDelta = (_leftController->GripDelta() + _rightController->GripDelta()) * 0.5;
		else if(_leftController->IsGripping())
			moveDelta = _leftController->GripDelta();
		else if(_rightController->IsGripping())
			moveDelta = _rightController->GripDelta();

		AddActorWorldOffset(moveDelta);
	}
}

void AVRCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	_playerInputComponent = PlayerInputComponent;

	// NOTE: Most controller bindings are handled in controller classes
	// Axis Bindings Give -1 in cases of backwards/left
	PlayerInputComponent->BindAxis(L"Move_Y", this, &AVRCharacter::MoveForward);
	PlayerInputComponent->BindAxis(L"Move_X", this, &AVRCharacter::MoveRight);
	// Action Bindings
	PlayerInputComponent->BindAction(L"CameraRaycast", EInputEvent::IE_Pressed, this, &AVRCharacter::CameraRaycast);
	PlayerInputComponent->BindAction(L"Teleport",EInputEvent::IE_Pressed,this,&AVRCharacter::StartCastingTeleportMarker);
	PlayerInputComponent->BindAction(L"Teleport",EInputEvent::IE_Released,this,&AVRCharacter::TeleportToDestinationMarker);
}

void AVRCharacter::MoveForward(float val)
{
	AddMovementInput(_camera->GetForwardVector(), val);
}

void AVRCharacter::MoveRight(float val)
{
	AddMovementInput(_camera->GetRightVector(), val);
}

void AVRCharacter::GripStart(AHandController* gripCon)
{
	_isClimbing = true;
}

void AVRCharacter::GripEnd(AHandController* gripCon)
{
	if(!_leftController->IsGripping() && !_rightController->IsGripping())
	{
		_isClimbing = false;

		FNavLocation projectedLoc;
		auto navSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
		if(navSys->ProjectPointToNavigation(GetActorLocation(), projectedLoc, {100,100,50}))
			ExecuteTeleport(projectedLoc.Location);
	}
}

void AVRCharacter::UpdateVRRoot()
{
	FVector rootToCamera = _camera->GetComponentLocation() - GetActorLocation();
	rootToCamera.Z = 0;

	AddActorWorldOffset(rootToCamera);
	
	_vrRoot->AddWorldOffset(-rootToCamera);
}

void AVRCharacter::UpdateBlinkers()
{
	if(_blinkerInstance && _radiusVsVelocity)
	{
		float curveVal = _radiusVsVelocity->GetFloatValue(GetVelocity().Size());
		_blinkerInstance->SetScalarParameterValue(L"Radius", curveVal);
		_blinkerInstance->SetVectorParameterValue(L"Center", FLinearColor{GetBlinkersCenter()});
	}
}

FVector AVRCharacter::GetBlinkersCenter()
{
	if(!_playerController || GetVelocity().IsNearlyZero())
		return FVector{0.5, 0.5, 0.0};

	FVector normal = GetVelocity().GetSafeNormal();

	// If we're going backwards, mirror the normal so it still is in user vision
	if(FVector::DotProduct(_camera->GetForwardVector(), normal) < 0)
		normal = -normal;

	FVector movVec {_camera->GetComponentLocation() + normal * 1000};
	FVector2D screenLoc;

	if(_playerController->ProjectWorldLocationToScreen(movVec, screenLoc))
	{
		int32 vpX, vpY;
		_playerController->GetViewportSize(vpX, vpY);
		return FVector{screenLoc.X / vpX, screenLoc.Y / vpY, 0.0f};
	}

	return FVector{0.5f, 0.5f, 0.0f};
}

void AVRCharacter::TeleportToDestinationMarker()
{
	ExecuteTeleport(_destinationMarker->GetComponentLocation());
	_isCastingTeleport = false;
	_destinationMarker->SetHiddenInGame(true, true);
	_teleportPath->SetHiddenInGame(true, true);
}

void AVRCharacter::ExecuteTeleport(const FVector& location)
{
	if(_isTeleporting || !_canTeleport)
		return;

	_isTeleporting = true;

	float tpHalfTime = _teleportTimeSeconds * 0.5f;
	FTimerHandle timer;

	_playerController->PlayerCameraManager->StartCameraFade(0.0, 1.0, tpHalfTime, FLinearColor::Black, true, true);

	GetWorldTimerManager().SetTimer(timer, [this, location, tpHalfTime]()
	{
		FVector loc = location;
		loc.Z += _actorHalfHeight;
		
		SetActorLocation(loc);
		
		FTimerHandle noworries;
		GetWorldTimerManager().SetTimer(noworries, [this, tpHalfTime]()
		{
			this->_playerController->PlayerCameraManager->StartCameraFade(1.0, 0.0, tpHalfTime, FLinearColor::Black, true, true);
			this->_isTeleporting = false;
		}, tpHalfTime, false);

	}, tpHalfTime, false);
}

void AVRCharacter::CastTeleportDestinationMarker()
{
	FHitResult hit;
	FVector start {_leftController->GetActorLocation()};
	FVector testVector {_leftController->GetActorForwardVector().RotateAngleAxis(30, _leftController->GetActorRightVector())};
	FVector forw {start + testVector * _teleportDistance};
	FNavLocation projectedLoc;
	auto navSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());

	// Teleport Path Spline
	FPredictProjectilePathResult predictResult;
	FPredictProjectilePathParams predictParams;
	predictParams.bTraceWithChannel = true;
	predictParams.bTraceWithCollision = true;
	predictParams.bTraceComplex = true;
	predictParams.TraceChannel = ECollisionChannel::ECC_Visibility;
	predictParams.LaunchVelocity = _leftController->GetActorForwardVector() * _teleportDistance;
	predictParams.StartLocation = start;
	predictParams.ProjectileRadius = 10.0f;
	predictParams.MaxSimTime = 5.0f;
	predictParams.SimFrequency = 5.0f;
	predictParams.ActorsToIgnore = {this};
	//predictParams.DrawDebugType = EDrawDebugTrace::ForOneFrame;

	ClearAndHideTeleportSpline();

	if(UGameplayStatics::PredictProjectilePath(GetWorld(), predictParams, predictResult))
	{
		if(navSys->ProjectPointToNavigation(predictResult.HitResult.Location, projectedLoc, {0,0,3000}))
		{
			UpdateTeleportSpline(predictResult.PathData);

			_destinationMarker->SetWorldLocation(projectedLoc.Location);
			_canTeleport = true;

			if (_destinationMarker->bHiddenInGame)
				_destinationMarker->SetHiddenInGame(false, true);

			return;
		}
	}
	
	
	if (!_destinationMarker->bHiddenInGame)
	{
		_destinationMarker->SetHiddenInGame(true, true);
		_canTeleport = false;
	}
}

void AVRCharacter::UpdateTeleportSpline(const TArray<FPredictProjectilePathPointData> & points)
{
	// Spline shenanigans for tp path
	int usedMeshes = 0;
	for(; usedMeshes < points.Num(); ++usedMeshes)
	{
		if(_teleportSplineMeshPool.Num() <= usedMeshes)
		{
			// Wow better name them specifically, seems the engine refers to them by string name!
			FString meshName{L"Teleport Arc #"};
			meshName += FString::FromInt(usedMeshes);

			USplineMeshComponent* arcPointMesh = NewObject<USplineMeshComponent>(this, *meshName);
			arcPointMesh->SetMobility(EComponentMobility::Movable);
			arcPointMesh->AttachToComponent(_teleportPath, FAttachmentTransformRules::KeepRelativeTransform);
			arcPointMesh->SetStaticMesh(_teleportArcMeshParent);
			arcPointMesh->SetMaterial(0, _teleportArcMaterial);
			arcPointMesh->RegisterComponent();

			_teleportSplineMeshPool.Add(arcPointMesh);
		}

		_teleportPath->AddSplinePoint(points[usedMeshes].Location, ESplineCoordinateSpace::World, false);
		_teleportPath->SetSplinePointType(usedMeshes, ESplinePointType::Curve, false);
		
		// Log Example
		//FString logStr{L"Loc: "};
		//logStr += FString::FromInt(points[i].Location.X);
		//logStr += L":";
		//logStr += FString::FromInt(points[i].Location.Y);
		//logStr += L":";
		//logStr += FString::FromInt(points[i].Location.Z);
		//logStr += L"--";
		//logStr += FString::FromInt(i);

		//UE_LOG(LogTemp, Warning, L"%s", *logStr);
	}

	// !!! Muy Importante !!!
	_teleportPath->UpdateSpline();

	// Bend those meshes!
	for(int i = 0; i < usedMeshes; ++i)
	{
		FVector start, startTan, end, endTan;
		_teleportPath->GetLocalLocationAndTangentAtSplinePoint(i, start, startTan);
		_teleportPath->GetLocalLocationAndTangentAtSplinePoint(i + 1, end, endTan);

		_teleportSplineMeshPool[i]->SetStartAndEnd(start, startTan, end, endTan, true);
		_teleportSplineMeshPool[i]->SetHiddenInGame(false, true);
	}
}

void AVRCharacter::ClearAndHideTeleportSpline()
{
	if(_teleportPath->GetNumberOfSplinePoints())
		_teleportPath->ClearSplinePoints();

	for(USplineMeshComponent* mesh : _teleportSplineMeshPool)
		mesh->SetHiddenInGame(true, true);
}

void AVRCharacter::CameraRaycast()
{
	float castMag {20000};
	FHitResult hit;
	FVector start {_camera->GetComponentLocation()};
	FVector forw {start + _camera->GetForwardVector() * castMag};
	FVector up  {start + _camera->GetUpVector() * castMag};
	FVector right {start + _camera->GetRightVector() * castMag};
	
	DrawDebugLine(GetWorld(), start, forw, FColor::Green, false, 5.0f, 0, 1.0f);
	DrawDebugLine(GetWorld(), start, up, FColor::Blue, false, 5.0f, 0, 2.0f);
	DrawDebugLine(GetWorld(), start, right, FColor::Red, false, 5.0f, 0, 2.0f);
}