// Fill out your copyright notice in the Description page of Project Settings.


#include "HandController.h"

#include "MotionControllerComponent.h"
#include "HeadMountedDisplay.h"

#include "Kismet/GameplayStatics.h" 

// Sets default values
AHandController::AHandController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	_controller = CreateDefaultSubobject<UMotionControllerComponent>(L"ControllerComponent");
	SetRootComponent(_controller);
}

// Called when the game starts or when spawned
void AHandController::BeginPlay()
{
	Super::BeginPlay();
	
	FScriptDelegate onOverlap;
	onOverlap.BindUFunction(this, L"ActorBeginOverlap");
	OnActorBeginOverlap.Add(onOverlap);
	
	FScriptDelegate onOverlapEnd;
	onOverlapEnd.BindUFunction(this, L"ActorEndOverlap");
	OnActorEndOverlap.Add(onOverlapEnd);;
}

void AHandController::PostInitProperties()
{
	Super::PostInitProperties();

	_controller->SetTrackingSource(_controllerHand);
}

void AHandController::SetupControllerBindings(UInputComponent* PlayerInputComponent)
{
	_playerInputComponent = PlayerInputComponent;

	// Action Bindings
	switch(_controllerHand)
	{
	case EControllerHand::Left:
		PlayerInputComponent->BindAction(L"GripLeft",EInputEvent::IE_Pressed,this,&AHandController::Grip);
		PlayerInputComponent->BindAction(L"GripLeft",EInputEvent::IE_Released,this,&AHandController::Release);
		break;
	case EControllerHand::Right:
		PlayerInputComponent->BindAction(L"GripRight",EInputEvent::IE_Pressed,this,&AHandController::Grip);
		PlayerInputComponent->BindAction(L"GripRight",EInputEvent::IE_Released,this,&AHandController::Release);
		break;
	default:
		UE_LOG(LogTemp, Warning, L"%s", L"Attempted to bind to an interdimensional hand.");
		return;
	};
}

// Called every frame
void AHandController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(_isGripping)
		_gripDelta = _gripLocation - GetActorLocation();
}

void AHandController::Grip()
{
	UE_LOG(LogTemp, Warning, L"%s", L"Grip");
	if(_canClimb)
	{
		_isGripping = true;
		_gripLocation = GetActorLocation();
		OnGripStart.ExecuteIfBound(this);
	}
}

void AHandController::Release()
{
	UE_LOG(LogTemp, Warning, L"%s", L"Release");
	if(_isGripping)
	{
		_isGripping = false;
		_gripLocation = FVector{0,0,0};
		OnGripEnd.ExecuteIfBound(this);
	}
}

void AHandController::ActorBeginOverlap(AActor* overlapped, AActor* other)
{
	if(other)
	{
		FString name {"Other: "};
		name += AActor::GetDebugName(other);
		UE_LOG(LogTemp, Warning, L"%s", *name);
	}

	if(!_canClimb && !_isGripping)
		if(other->ActorHasTag(L"Climbable"))
		{
			UE_LOG(LogTemp, Warning, L"%s", L"Can Climb!");
			_canClimb = true;

			_playerController->PlayHapticEffect(_canClimbRumble, _controllerHand);
		}
}

void AHandController::ActorEndOverlap(AActor* overlapped, AActor* other)
{
	if(_isGripping || !_canClimb)
		return;

	if(other)
	{
		FString name {"Other: "};
		name += AActor::GetDebugName(other);
		UE_LOG(LogTemp, Warning, L"%s", *name);
	}

	TArray<AActor*> actors;
	GetOverlappingActors(actors);
	for(AActor* actor : actors)
		if(actor->Tags.Contains(L"Climbable"))
			return;

	UE_LOG(LogTemp, Warning, L"%s", L"STOP Climb!");
	_canClimb = false;
}
