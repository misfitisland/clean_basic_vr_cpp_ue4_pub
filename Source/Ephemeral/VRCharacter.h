// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "HandController.h"

#include "VRCharacter.generated.h"

UCLASS()
class EPHEMERAL_API AVRCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AVRCharacter();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	// Locomotion
	void MoveForward(float val);
	void MoveRight(float val);
	
	// Delegates
	UFUNCTION()
		void GripStart(AHandController* gripCon);
	UFUNCTION()
		void GripEnd(AHandController* gripCon);

	// Teleportation
	void CastTeleportDestinationMarker();
	void TeleportToDestinationMarker();
	void ExecuteTeleport(const FVector& location);
	void UpdateTeleportSpline(const TArray<struct FPredictProjectilePathPointData> & points);
	void ClearAndHideTeleportSpline();
	void StartCastingTeleportMarker() {_isCastingTeleport = true;}

	// Character Specifics
	void UpdateVRRoot();
	void UpdateBlinkers();
	FVector GetBlinkersCenter();

	// Debug
	void CameraRaycast();

	// Members
	float _actorHalfHeight {0.0f};
	bool  _canTeleport {false};
	bool _isCastingTeleport{false};
	bool _isTeleporting {false};
	bool _isClimbing {false};
	
	// *** UProperties ***
	// Guts
	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* _camera {nullptr};
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* _vrRoot {nullptr};
	UPROPERTY(VisibleAnywhere)
		class APlayerController* _playerController {nullptr};

	// Controllers and Input
	UPROPERTY()
		class UInputComponent* _playerInputComponent;
	UPROPERTY(VisibleAnywhere)
		AHandController* _leftController;
	UPROPERTY(VisibleAnywhere)
		AHandController* _rightController;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AHandController> _leftHandControllerBP;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AHandController> _rightHandControllerBP;

	// Movement Blinkers
	UPROPERTY()
		class UPostProcessComponent* _postProcessComponent {nullptr};
	UPROPERTY(EditAnywhere)
		class UMaterialInterface* _blinkerMaterialBase {nullptr};
	UPROPERTY()
		class UMaterialInstanceDynamic* _blinkerInstance {nullptr};
	UPROPERTY(EditAnywhere)
		class UCurveFloat* _radiusVsVelocity {nullptr};

	// Teleportation
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* _destinationMarker {nullptr};
	UPROPERTY(VisibleAnywhere)
		class USplineComponent* _teleportPath;
	UPROPERTY(EditAnywhere)
		float _teleportDistance {1000};
	UPROPERTY(EditAnywhere)
		float _teleportTimeSeconds{0.5f};
	UPROPERTY(VisibleAnywhere)
		class UInstancedStaticMeshComponent* _teleportArcMesh;
	UPROPERTY(EditDefaultsOnly)
		class UStaticMesh* _teleportArcMeshParent;
	UPROPERTY(EditDefaultsOnly)
		class UMaterialInterface* _teleportArcMaterial;
	UPROPERTY(VisibleAnywhere)
		TArray<class USplineMeshComponent*> _teleportSplineMeshPool;
};
