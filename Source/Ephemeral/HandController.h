// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

//#include "Delegates/DelegateCombinations.h"

#include "GameFramework/Actor.h"

#include "HandController.generated.h"

UCLASS()
class EPHEMERAL_API AHandController : public AActor
{
	GENERATED_BODY()
	
	DECLARE_DELEGATE_OneParam(ControllerGripStartDelegate, AHandController*);
	DECLARE_DELEGATE_OneParam(ControllerGripEndDelegate, AHandController*);

public:	
	AHandController();

	virtual void Tick(float DeltaTime) override;

	void SetupControllerBindings(class UInputComponent* PlayerInputComponent);

	// Accessors/Mutators
	bool IsGripping() {return _isGripping;}
	FVector GripDelta() {return _gripDelta;}

	// Actions
	void Grip();
	void Release();

	// Delegates
	ControllerGripStartDelegate OnGripStart;
	ControllerGripStartDelegate OnGripEnd;

protected:
	virtual void BeginPlay() override;
	virtual void PostInitProperties() override;

	UFUNCTION()
		void ActorBeginOverlap(AActor* overlapped, AActor* other);
	UFUNCTION()
		void ActorEndOverlap(AActor* overlapped, AActor* other);

private:

	// Members
	bool _canClimb {false};
	bool _isGripping {false};
	FVector _gripLocation;
	FVector _gripDelta;
	float _currentOverlapWindow {0.1f};
	float _lastOverlapEventTime {0.0f};

	// *** UProperties ***
	// Guts
	UPROPERTY()
		class APlayerController* _playerController {nullptr};
	UPROPERTY()
		class UInputComponent* _playerInputComponent;
	UPROPERTY(EditDefaultsOnly)
		EControllerHand _controllerHand;
	UPROPERTY(VisibleAnywhere)
		class UMotionControllerComponent* _controller {nullptr};
	UPROPERTY(EditDefaultsOnly)
		class UHapticFeedbackEffect_Base* _canClimbRumble {nullptr};
};
